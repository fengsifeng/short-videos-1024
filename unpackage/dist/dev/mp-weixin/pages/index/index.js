"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  data() {
    return {
      safeArea: 0,
      // 底部安全区
      windowWidth: 0,
      //屏幕宽度
      windowHeight: 0,
      //屏幕高度
      screenWidth: 0,
      // videoHeight: 0, //视频部分高度
      // videoWidth: 0, //视频部分宽度
      video_list: [],
      test_allImgList: [
        { "status": "1", "data": 1 },
        { "status": "1", "data": 2 },
        { "status": "1", "data": 3 },
        { "status": "1", "data": 4 },
        { "status": "1", "data": 5 },
        { "status": "0", "data": 6 },
        { "status": "0", "data": 7 },
        { "status": "0", "data": 8 },
        { "status": "0", "data": 9 },
        { "status": "0", "data": 10 },
        { "status": "0", "data": 11 }
      ],
      pay_data_length: 0,
      pay_data_1: [
        { "label": "超值", "money": "99.88", "golds": "1000金币+500金币", "give": "多送88元" },
        { "label": "超值", "money": "999.88", "golds": "888金币+520金币", "give": "多送99元" },
        { "label": "", "money": "888.88", "golds": "250金币+890金币", "give": "" },
        { "label": "超值", "money": "19.88", "golds": "25金币+450金币", "give": "多送18元" },
        { "label": "超值", "money": "1.88", "golds": "50金币+500金币", "give": "多送98元" },
        { "label": "超值", "money": "1.88", "golds": "50金币+500金币", "give": "多送98元" },
        { "label": "超值", "money": "1.88", "golds": "50金币+500金币", "give": "多送98元" }
      ],
      pay_data_2: [
        { "label": "超值", "money": "99.88", "golds": "1000金币+500金币", "give": "多送88元" },
        { "label": "超值", "money": "999.88", "golds": "888金币+520金币", "give": "多送99元" }
      ],
      pay_height_data: [0.33, 0.5, 0.64],
      //充值模板，<=2、 <=4、>4的模板高度选项
      pay_height_per: 0
    };
  },
  onLoad() {
    common_vendor.index.getSystemInfo({
      success: (res) => {
        this.safeArea = res.safeAreaInsets.bottom;
      }
    });
    this.screenWidth = common_vendor.index.getSystemInfoSync().screenWidth;
    this.windowWidth = common_vendor.index.getSystemInfoSync().windowWidth;
    this.windowHeight = common_vendor.index.getSystemInfoSync().windowHeight - this.safeArea;
    this.windowWidth = 750 * this.windowWidth / this.screenWidth;
    this.windowHeight = 750 * this.windowHeight / this.screenWidth;
    console.log(this.screenWidth, this.safeArea, this.windowHeight, this.windowWidth);
    this.getVideoList();
    common_vendor.index.setNavigationBarTitle({
      //测试使用，正式使用在页面启动前获取数据
      title: "XXX剧"
    });
    this.pay_data_length = this.pay_data_1.length;
    if (this.pay_data_length <= 2) {
      this.pay_height_per = this.pay_height_data[0];
    } else if (this.pay_data_length <= 4) {
      this.pay_height_per = this.pay_height_data[1];
    } else {
      this.pay_height_per = this.pay_height_data[2];
    }
  },
  methods: {
    getVideoList() {
      this.video_list = [
        {
          "id": 1,
          "url": "https://image.szytcm8.com/image/20231024/vf1446959.mp4"
        },
        {
          "id": 2,
          "url": "https://image.szytcm8.com/image/20231024/vf1446959.mp4"
        },
        {
          "id": 3,
          "url": "https://image.szytcm8.com/image/20231024/vf1446959.mp4"
        }
      ];
    },
    toggleEp(type) {
      this.$nextTick(() => {
        this.$refs.ep.open(type);
      });
    },
    togglePay(type) {
      this.$nextTick(() => {
        this.$refs.pay.open(type);
      });
    },
    change(event) {
      console.log(event);
      common_vendor.index.setNavigationBarTitle({
        //测试使用，正式使用在页面启动前获取数据
        title: "XXX剧 第" + event.detail.current + "集"
      });
    }
  }
};
if (!Array) {
  const _easycom_uni_popup2 = common_vendor.resolveComponent("uni-popup");
  _easycom_uni_popup2();
}
const _easycom_uni_popup = () => "../../uni_modules/uni-popup/components/uni-popup/uni-popup.js";
if (!Math) {
  _easycom_uni_popup();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.f($data.video_list, (item, index, i0) => {
      return {
        a: item.url,
        b: item.id,
        c: index
      };
    }),
    b: common_vendor.s("width: " + $data.windowWidth + "rpx; height: " + $data.windowHeight + "rpx;"),
    c: common_vendor.s("width: " + $data.windowWidth + "rpx; height: " + $data.windowHeight + "rpx; background-color: #000;"),
    d: common_vendor.o((...args) => $options.change && $options.change(...args)),
    e: common_vendor.o(($event) => $options.togglePay("bottom")),
    f: common_vendor.o(($event) => $options.toggleEp("bottom")),
    g: common_vendor.f($data.test_allImgList, (value, index, i0) => {
      return common_vendor.e({
        a: value.status == 1
      }, value.status == 1 ? {} : {}, {
        b: common_vendor.t(value.data),
        c: index
      });
    }),
    h: common_vendor.o((...args) => _ctx.lower && _ctx.lower(...args)),
    i: common_vendor.s("width: " + $data.windowWidth + "rpx; height: " + $data.windowHeight * 0.645 + "rpx; background-color: #fff; border-top-left-radius: 10px; border-top-right-radius: 10px;"),
    j: common_vendor.sr("ep", "4cb04495-0"),
    k: common_vendor.p({
      ["background-color"]: "#fff"
    }),
    l: common_vendor.f($data.pay_data_1, (value, index, i0) => {
      return common_vendor.e({
        a: value.label
      }, value.label ? {
        b: common_vendor.t(value.label)
      } : {
        c: common_vendor.t(value.label)
      }, {
        d: common_vendor.t(value.money),
        e: common_vendor.t(value.golds),
        f: value.give
      }, value.give ? {
        g: common_vendor.t(value.give)
      } : {}, {
        h: index
      });
    }),
    m: common_vendor.o((...args) => _ctx.lower && _ctx.lower(...args)),
    n: common_vendor.s("width: " + $data.windowWidth + "rpx; height: " + $data.windowHeight * $data.pay_height_per + "rpx; background-color: #fff; border-top-left-radius: 10px; border-top-right-radius: 10px;"),
    o: common_vendor.sr("pay", "4cb04495-1"),
    p: common_vendor.p({
      ["background-color"]: "#fff"
    })
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "D:/short-videos-1024/pages/index/index.vue"]]);
wx.createPage(MiniProgramPage);
